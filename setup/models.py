from django.db import models

# Contact Types - e.g. Vendor, Contractor, Hauler, etc. - THT 02/12/2019
class ContactType(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    description = models.CharField(max_length=200, null=True, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

# Phone Types - e.g. Office, Cell, Fax, etc. - THT 02/12/2019
class PhoneType(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    description = models.CharField(max_length=200, null=True, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

# Address Types - e.g. Office, Home, P.O. Box, etc. - THT 02/12/2019
class AddressType(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    description = models.CharField(max_length=200, null=True, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

# Email Types - e.g. Office, Personal, etc. - THT 02/12/2019
class EmailType(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    description = models.CharField(max_length=200, null=True, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

# Liability Types - e.g. Workers Comp, Auto, etc. - THT 02/12/2019
class LiabilityType(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    description = models.CharField(max_length=200, null=True, blank=True)
    order = models.IntegerField(default=0)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

