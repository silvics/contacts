from django.contrib import admin
from .models import *

class ContactTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'active']

class PhoneTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'active']

# Address Types - e.g. Office, Home, P.O. Box, etc. - THT 02/12/2019
class AddressTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'active']

# Email Types - e.g. Office, Personal, etc. - THT 02/12/2019
class EmailTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'active']

# Liability Types - e.g. Workers Comp, Auto, etc. - THT 02/12/2019
class LiabilityTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'order', 'active']

admin.site.register(ContactType, ContactTypeAdmin)
admin.site.register(PhoneType, PhoneTypeAdmin)
admin.site.register(AddressType, AddressTypeAdmin)
admin.site.register(EmailType, EmailTypeAdmin)
admin.site.register(LiabilityType, LiabilityTypeAdmin)