from django.contrib import admin
from .models import Contact, Address

class ContactAdmin(admin.ModelAdmin):
    change_list_title = 'Select Contact to Change'
    fieldsets = [
        (None, {'fields': [('first_name', 'last_name',), ('dba_name', 'title',), ('active',), ('archive',)]})
    ]
    list_display = ['full_name', 'title', 'active']

class AddressAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': [('contact', 'address_type'), ('address_1',), ('address_2',), ('city', 'state', 'zip_code',), ('country',), ('active',)]})
    ]
    list_display = ['address_1', 'address_2', 'city', 'state', 'zip_code', 'contact', 'address_type', 'active']

admin.site.register(Contact, ContactAdmin)
admin.site.register(Address, AddressAdmin)