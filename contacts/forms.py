from django.forms import ModelForm
from .models import Contact, Address


class ContactForm(ModelForm):

    class Meta:
        model = Contact
        fields = '__all__'


class AddressForm(ModelForm):

    class Meta:
        model = Address
        fields = '__all__'
