from django.urls import path
from . import views

app_name = 'contacts'
urlpatterns = [
    path('', views.ContactListView.as_view(), name='index'),
    path('<int:pk>/', views.ContactDetailView.as_view(), name='contact_detail'),
    path('<int:contact_id>/update/', views.ContactUpdate, name='contact_update'),
    path('address/', views.AddressListView.as_view(), name='address_list'),
    path('address/<int:pk>/', views.AddressDetailView.as_view(), name='address_detail'),
    path('address/<int:address_id>/update/', views.AddressUpdate, name='address_update'),
]
