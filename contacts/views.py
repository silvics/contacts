from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

from .models import Contact, Address
from .forms import ContactForm, AddressForm

# Basic landing page for the contacts
class ContactListView(ListView):
    model = Contact
    context_object_name = 'contacts_list'
    template_name = 'contacts/index.html'

class AddressListView(ListView):
    model = Address

#
class ContactDetailView(UpdateView):
    form_class = ContactForm
    model = Contact
    template_name = 'contacts/contact_detail.html'

class AddressDetailView(UpdateView):
    form_class = AddressForm
    model = Address
    template_name = 'contacts/address_detail.html'

def ContactUpdate(request, contact_id):
    contact = get_object_or_404(Contact, pk=contact_id)
    if request.method == 'POST':
        try:
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            title = request.POST['title']
            dba_name = request.POST['dba_name']
            print(Contact.objects.get(pk=request.POST['parent']))
            parent = Contact.objects.filter(pk=request.POST['parent']).first()
            active = request.POST.get('active', '') == 'on'
            archive = request.POST.get('archive', '') == 'on'
        except:
            return render(request, 'contacts/contact_detail.html', {
                'contact':contact,
                'error_message': "Something went wrong. Fix it. Or don't. I'm a badly written django project, not your dad..."
                })
        else:
            contact.first_name = first_name
            contact.last_name = last_name
            contact.title = title
            contact.dba_name = dba_name
            contact.active = active
            contact.archive = archive
            contact.parent = parent
            contact.save()
            return HttpResponseRedirect(reverse('contacts:index'))


def AddressUpdate(request, address_id):
    address = get_object_or_404(Address, pk=address_id)
    if request.method == 'POST':
        try:
            address1 = request.POST['address_1']
            address2 = request.POST['address_2']
            city = request.POST['city']
            state = request.POST['state']
            zip_code = request.POST['zip_code']
            country = request.POST['country']
        except:
            return render(request, 'contacts/address_detail.html', {
                'address':address,
                'error_message': "Something went wrong. Fix it. Or don't. I'm a badly written django project, not your dad..."
                })
        else:
            address.address_1 = address1
            address.address_2 = address2
            address.city = city
            address.state = state
            address.zip_code = zip_code
            address.country = country
            address.save()
            return HttpResponseRedirect(reverse('contacts:index'))

