from django.db import models
from setup.models import *

# Base Contact Class and data model - THT 02/12/2019
class Contact(models.Model):
    first_name = models.CharField(max_length=100, null=False, blank=False, verbose_name='First Name')
    last_name = models.CharField(max_length=100, null=True, default='', verbose_name='Last Name')
    dba_name = models.CharField(max_length=200, null=True, blank=True, verbose_name = 'DBA')
    title = models.CharField(max_length=100, null=True, blank=True)
    parent = models.ForeignKey('Contact', null=True, blank=True, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    archive = models.BooleanField(default=False)

    def __str__(self):
        if self.last_name:
            return '{0} {1}'.format(self.first_name, self.last_name)
        else:
            return self.first_name

    def full_name(self):
        if self.last_name:
            return '{0} {1}'.format(self.first_name, self.last_name)
        else:
            return self.first_name

# Since a contact can have multiple roles depending on the job, we use XType to
# form a relation between types and contacts. As each type may be a different
# vendor, the vendor_num is stored here. - THT 02/12/2019
class XType(models.Model):
    contact = models.ForeignKey('Contact', null=False, blank=False, on_delete=models.CASCADE)
    contact_type = models.ForeignKey(ContactType, null=False, blank=False, on_delete=models.CASCADE)
    description = models.CharField(max_length=200, null=True, blank=True)
    vendor_num = models.CharField(max_length=100, null=True, blank=True, verbose_name='Vendor Number')
    active = models.BooleanField(default=True)

    def __str__(self):
        if self.last_name:
            return '{0} {1}, {2}'.format(self.contact.first_name, self.contact.last_name, self.contact_type.name)
        else:
            return '{0}, {1}'.format(self.contact.first_name, self.contact_type.name)


# Phone numbers associated with the contact. - THT 02/12/2019
class Phone(models.Model):
    contact = models.ForeignKey('Contact', null=False, blank=False, on_delete=models.CASCADE)
    phone_type = models.ForeignKey(PhoneType, null=False, blank=False, on_delete=models.CASCADE)
    phone = models.CharField(max_length=50, null=False, blank=False, verbose_name='Phone Number')
    primary = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    def __str__(self):
        return '{0} ({1})'.format(self.phone, self.phone_type.name)

# Addresses associated with the contact. - THT 02/12/2019
class Address(models.Model):
    contact = models.ForeignKey('Contact', null=False, blank=False, on_delete=models.CASCADE)
    address_type = models.ForeignKey(AddressType, null=False, blank=False, on_delete=models.CASCADE)
    address_1 = models.CharField(max_length=100, null=True, blank=True)
    address_2 = models.CharField(max_length=100, null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    state = models.CharField(max_length=50, null=True, blank=True)
    zip_code = models.CharField(max_length=20, null=True, blank=True)
    country = models.CharField(max_length=100, null=True, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        if self.address_2:
            return '{0}: {1}, {2}, {3}, {4} {5}'.format(self.address_type.name, self.address_1, self.address_2, self.city, self.state, self.zip_code)
        else:
            return '{0}: {1}, {2}, {3} {4}'.format(self.address_type.name, self.address_1, self.city, self.state, self.zip_code)

# Email addresses associated with the contact. - THT 02/12/2019
class Email(models.Model):
    contact = models.ForeignKey('Contact', null=False, blank=False, on_delete=models.CASCADE)
    email_type = models.ForeignKey(EmailType, null=False, blank=False, on_delete=models.CASCADE)
    email_address = models.CharField(max_length=50, null=False, blank=False, verbose_name='Email Address')
    primary = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    def __str__(self):
        return '{0}, {1}'.format(self.email_address, self.email_type.name)

# Liability information associated with the contact - THT 02/12/2019
class Liability(models.Model):
    contact = models.ForeignKey('Contact', null=False, blank=False, on_delete=models.CASCADE)
    liability_type = models.ForeignKey(LiabilityType, null=False, blank=False, on_delete=models.CASCADE)
    policy = models.CharField(max_length=100, null=True, blank=True)
    exp_date = models.DateField(null=True, blank=True, verbose_name='Expiration Date')
    limit = models.DecimalField(max_digits=18, decimal_places=2, null=True, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.liability_type.name